# (**J**)ava (**E**)di (**T**)ask (**S**)cheduler
## JETS Scheduler
 *JETS* is a application made in Java application used to run daily schedules read from a table. Schedules are defined by the JETSContext.xml. The application uses Spring Framework and Cron schedules to operate. All schedules are concurrent of each other and there is logging for each each schedule. The JETSContext.xml is required and is automatically created at the start of the program if the file does not already exist.The JETSContext.xml file looks for six items:  
* **Schedule Table** - Table that the application reads to know what schedules to run at what times  
* **On Demand Table** - Table to run schedules in the next 5 minute run, whether on the 0 or 5.  
* **Main Email Table** - A email distribution  if a error occurs in the program or any scripts ran in the schedule, people are emailed.  
* **Scripts Directory** - Location where all the Scripts are kept.  
* **Calendar Directory** - Location where all the Calendar Files are kept  
* **Log Directory** - Location where you would like your logs for the application to go  

 **Features:**  
 - Automatic 25 minute Timeout on each script  
 - Runs Batch, Executables, Perl, and Java.   
 - Emailing on Application Errors and Script Errors  
 - Runs in Parallel(Scripts in Schedule are kicked off at the same time) 
 
## appContext.xml
JETS Scheduler uses a need a XML file called *appContext.xml* to run . In the XML, the above parameters must be stated. See Below.
### Parameters for Application in appContext.xml
	<bean id="ScheduleParams" class = "com.edi.sch.ScheduleComponent" >  
	  <constructor-arg index="0" value="C:\Users\63530\Projects\scheduler\JETS\schedule\JETS_SchTable.dat"/>     <!--Schedule Table-->  
	  <constructor-arg index="1" value="C:\Users\63530\Projects\scheduler\JETS\schedule\JETS_SchOnDemand.dat"/>  <!--OnDemand Table-->  
	  <constructor-arg index="2" value="C:\Users\63530\Projects\scheduler\\Email\JETS0001.txt"/>                 <!--Primary Email Table for Errors-->  
	  <constructor-arg index="3" value="C:\scripts\"/>                                                           <!--Script Directory-->  
	  <constructor-arg index="4" value="C:\Users\63530\Projects\scheduler\JETS\schedule\Calendars\"/>            <!--Log Directory-->  
	  <constructor-arg index="5" value="C:\Users\63530\Projects\scheduler\JETS\logs\"/>                          <!--Email Tables Directory-->  
	</bean>  


### Cron Timing for Schedules in appContext.xml
	<task:scheduled-tasks scheduler="JETS">  
	  <task:scheduled ref="mainBean" method="UpdateScheduler"   cron="30 * * * * ?"/>                          <!--KEEP-->  
	  <task:scheduled ref="mainBean" method="executeDaily"      cron="1 0/5 * * * ?" />                         <!--D-->  
	  <task:scheduled ref="mainBean" method="executeYearly"     cron="1 0/5 * * * ?"/>                          <!--Y-->  
	  <task:scheduled ref="mainBean" method="executeMonthly"    cron="1 0/5 * * * ?"/>                          <!--M-->  
	  <task:scheduled ref="mainBean" method="executeWeekly"     cron="1 0/5 * * * ?"/>                          <!--W-->  
	  <task:scheduled ref="mainBean" method="executeOnTheZero"  cron="1 0,10,20,30,40,50 * * * ?"/>             <!--0-->  
	  <task:scheduled ref="mainBean" method="executeOnTheFive"  cron="1 5,15,25,35,45,55 * * * ?"/>             <!--5-->  
	  <task:scheduled ref="mainBean" method="executeOnDemand"   cron="1 0/5 * * * ?"/>                          <!--5-->  
	</task:scheduled-tasks>  

	<task:scheduler id="JETS" pool-size="10"/>  
	
  Note: DO NOT change the UpdateScheduler time. Keep it as it is..
### JETS Schedule Table Format Example
	#TimeQual|Day|Time|Script|Description  
	D|xx|0820|Script1.bat|Comment Example  
	M|15|1500|Script2.pl|Comment Example    
	0|xx|xxxx|TST_STORM.pl|JETS Storm Testing    
	#5|xx|xxxx|Script4.exe|Comment Example  
	#W|4|1600|Script5.exe|Comment Example   
	#Y|318|1425|Script6.pl|Comment Example   
 Lines starting with # are ignored in the program.


### TimeQual
- D - Daily     (D!xx!0820!Script1.bat!Test Table : Description - Execute Everyday at 8:20AM)  
- M - Monthly   (M!15!1500!Script2.pl!Test Table: Description - Execute on the 15th of Every Month at 3:00PM)  
- W - Weekly    (W!4!0020!Script3.exe!Test Table: Description - Execute on Every Wednesday at 12:20AM)  
- Y - Yearly    (Y!319!1424!quicktest5.pl!Test Table : Description - Execute on the 319th day of the year...November 15, 2018) 
- 0 - Zeros     (0!xx!xxxx!quicktest0.pl!Comment Example : Description - Execute on the Zeros (e.g. 5:00,5:10,5:20,5:30,5:40,5:50,6:00,..)
- 5 - Fives     (5!xx!xxxx!quicktest5.pl!Comment Example : Description - Execute on the Fives (e.g. 5:05,5:15,5:25,5:35,5:45,5:55,6:05,..)  
- B - Business  (B!xx!1530!quicktest4.pl!Test Business : Description - Execute during Business days (Monday - Friday)  
- C - Calendar  (C!ACCOUNTING!2255!quicktest3.pl!Test Business : Description - Execute schedule based on a Calendar (See Below for more information)  

For Weekly Scheudles: Sunday - 1, Monday - 2 Tuesday - 3,Wednesday - 4, Thursday - 5,Friday - 6, Saturday - 7.  
For the purpose of the README.md file, I have changed the Pipe in the Example Detail to the Exclamation point. Bitbucket will not display the PIPE correctly.  


### Calendar Tables Details

#### Calendar Table Format  
Year|Day Of Year|Description (Do not include this in your table file)  
2019|35|JAN-dd  
2019|63|FEB-dd  
2019|140|MAR-dd  
2019|126|APR-dd  
2019|154|MAY-dd  
2019|182|JUN-dd  
2019|217|JUL-dd  
2019|245|AUG-dd  
2019|273|SEP-dd  
2019|308|OCT-dd  
2019|336|NOV-dd  
2019|365|DEC-dd  

 - For Schedules on a Calendar, the second position indicates the subname of the Calendar. In the above example ACCOUNTING is the sub-name. 
   The sub-name MUST be prefixed with "SON_JETSCal_" and end in ".dat" (e.g. SON_JETSCal_ACCOUNTING.dat).   
 - All Calendar files must be placed in the Calendar Directory Folder.
 
### On Demand Table
- The On Demand table is a text file that the application reads to run Scripts on the next run when the user inputs a script. After running, the file is emptied.  

####Notes:
- Daily,Monthly,Weekly,Business,Calendar, and Yearly Schedule kick-offs are ran every five minutes. Validation to run the schedule are read from the ScheduleTable  
- Use Full script names in the schedule. (e.g. JavaProgram.class, Test1.bat).  
- Other programs, like the ones already in the application, can be called through Command-line batch.For example, if you would like to run Python Code, create a batch Script and call it.  
 
 
### MainEmail File Format  
 |User1.williams@site.com,User2.williams@site.com|User3.williams@site.com|
 
  Note: From|To|CC|BCC

### Helper Sites
* [Quartz .Net Scheduler] - The Spring Framework provides abstractions for asynchronous execution and scheduling of tasks!
* [Cron Examples] -The quick and simple editor for cron schedule expressions by Cronitor!


[Quartz .Net Scheduler]: <https://spring.io/guides/gs/scheduling-tasks/>
[Cron Examples]: <https://crontab.guru/examples.html>