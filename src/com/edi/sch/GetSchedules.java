package com.edi.sch;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


@Component
@Scope("prototype")
public class GetSchedules
{

    //public ArrayList<Schedule> toRun = new ArrayList<>();
    public String timer;


    public GetSchedules(String frequency) throws IOException
    {
        this.timer = frequency;

    }

    public ArrayList<Schedule> getSchedules(String NowDate, String NowTime,ArrayList<Schedule> schs)
    {
        ArrayList<Schedule> toRun = new ArrayList<>();
        Iterator<Schedule> schIterator = schs.iterator();
        while (schIterator.hasNext()) {
            Schedule sch = schIterator.next();
            if (timer.equals("D") || timer.equals("Y") || timer.equals("M") || timer.equals("W")) {
                if (timer.equals(sch.getTimeQual())) {
                    if (sch.runDate.equals(NowDate) && sch.runTime.equals(NowTime)) {
                        toRun.add(sch);
                    }
                }
            } else if (timer.equals("0") && timer.equals(sch.getTimeQual())) {
                if (NowTime.endsWith("0")) {
                    toRun.add(sch);
                }
            } else if (timer.equals("5") && timer.equals(sch.getTimeQual())) {
                if (NowTime.endsWith("5")) {
                    toRun.add(sch);
                }
            }
        }
        return toRun;
    }

    public ArrayList<Schedule> getCalendarSchedules(String NowDate, String NowTime, ArrayList<Schedule> schs) throws ScheduleException
    {
        Calendar cal = Calendar.getInstance();
        String DOY = String.valueOf(cal.get(Calendar.DAY_OF_YEAR));
        String CurrentYear = String.valueOf(cal.get(Calendar.YEAR));

        ArrayList<Schedule> toRun = new ArrayList<>();
        Iterator<Schedule> schIterator = schs.iterator();
        while (schIterator.hasNext()) {
            Schedule sch = schIterator.next();
            if (sch.getTimeQual().equals("C")) {
                File CalendarFile = new File(ScheduleComponent.CalendarDirectory + "SON_JETSCal_" + sch.calendarName + ".dat");

                try {
                    BufferedReader br = new BufferedReader(new FileReader(CalendarFile));
                    String line;
                    while ((line = br.readLine()) != null) {
                        if (!line.startsWith("#") || !line.trim().equals("")) {
                            String sYear = line.split("\\|")[0];
                            String sDOY = line.split("\\|")[1];
                            if (sch.runTime.equals(NowTime) && sDOY.equals(DOY) && sYear.equals(CurrentYear)) {
                                toRun.add(sch);

                            }
                        }
                    }

                    br.close();
                } catch (FileNotFoundException ex) {
                    throw new ScheduleException("Cannot Find Calender File " + CalendarFile.getName());
                } catch (IOException ex) {

                }


            }
        }
        return toRun;
    }

}


