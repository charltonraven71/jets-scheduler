package com.edi.sch;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class LogThread extends Thread
{
    private static final LogThread instance = new LogThread();
    private BlockingQueue<LogMessage> itemsToLog = new ArrayBlockingQueue<>(4);
    private static final LogMessage SHUTDOWN_REQ = new LogMessage("TERMINATE", "");
    boolean loggerTerminated;
    boolean shuttingDown;

    public LogThread()
    {
        start();

    }

    public void log(LogMessage log)
    {
        if (shuttingDown || loggerTerminated) return;
        try {
            // System.out.println("Putting Into " + log.filename);
            itemsToLog.put(log);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException("Unexepcted interruption");
        }
    }

    @Override
    public void run()
    {
        try {
            LogMessage log;
            while ((log = itemsToLog.take()) != SHUTDOWN_REQ) {
                Thread.sleep(10);
                LogWriter.WriteLine(log);
                // System.out.println("Writing to " + log.filename);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
         //   loggerTerminated = true;
        }
    }

    public void shutDown() throws InterruptedException
    {
        shuttingDown = true;
        itemsToLog.put(new LogMessage("TERMINATE", ""));
        System.out.println("Terminated");

    }
}
