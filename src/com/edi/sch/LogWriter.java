package com.edi.sch;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
public class LogWriter
{
    public static void WriteLine(LogMessage Info){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(Info.filename,true));
            bw.write(Info.message);
            bw.newLine();
            bw.close();
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }
    public static void Write(LogMessage Info){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(Info.filename,true));
            bw.write(Info.message);
            bw.close();
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }


    public static boolean deleteLog(String filename){


        return new File(filename).delete();

    }
}

