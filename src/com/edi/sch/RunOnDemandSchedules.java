package com.edi.sch;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;

public class RunOnDemandSchedules
{
    String script;
    LogWriter logFile;
    String logFilename;

    public static PriorityQueue<String> LogQueue = new PriorityQueue<>();


    public RunOnDemandSchedules(ArrayList<String> schList, String filename)
    {

        for (String run : schList) {
            Runnable runnable = new RunOnDemandThread(run, filename);
            Thread thread = new Thread(runnable);
            thread.start();
        }
    }

    class RunOnDemandThread implements Runnable
    {

        String script;
        String logFilename;

        RunOnDemandThread(String script, String logFile)
        {
            this.script = script;
            this.logFilename = logFile;
        }

        @Override
        public void run()
        {
            SchedulerService.logThread.log(new LogMessage(logFilename, "Running Script: " + script + "-> " + Utilities.NowDate_MMddyyyy_hhmmss_a() + "   " + "ON Demand"));


            //Perl
            if (script.toLowerCase().endsWith(".pl")) {
                String command = "perl " + ScheduleComponent.ScriptDirectory + script;
                RunScript(command);
            }

            //Java
            else if (script.toLowerCase().endsWith(".class")) {
                RunJava();
            }


            //Python
            else if (script.toLowerCase().endsWith(".py")) {
                String command = "python " + ScheduleComponent.ScriptDirectory + script;
                RunScript(command);
            }

            //C#
            else if (script.toLowerCase().endsWith(".csc")) {
                String command = "csc " + ScheduleComponent.ScriptDirectory + script;
                RunScript(command);
            }

            //Executables
            else if (script.toLowerCase().endsWith(".exe")) {
                String command = ScheduleComponent.ScriptDirectory + script;
                RunScript(command);
            }

            //Batch
            else if (script.toLowerCase().endsWith(".bat")) {
                String command = ScheduleComponent.ScriptDirectory + script;
                RunScript(command);
            }
            //Bash (Unix)
            else if (script.toLowerCase().endsWith(".sh")) {
                String command = ScheduleComponent.ScriptDirectory + script;
                RunScript(command);
            }


            SchedulerService.logThread.log(new LogMessage(logFilename, "Script " + script + " Complete ->" + Utilities.NowDate_MMddyyyy_hhmmss_a() + "   " + "On Demand"));

        }


        public void RunScript(String command)
        {
            try {
                StringBuffer output = new StringBuffer();
                Process p;

                p = Runtime.getRuntime().exec(command);
                p.waitFor(25, TimeUnit.MINUTES);

                if (p.isAlive()) {
                    p.destroy();
                }

                if (p.exitValue() != 0) {
                    BufferedReader ErrorOutput = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                    StringBuffer stringBuffer = new StringBuffer();
                    String s;
                    while ((s = ErrorOutput.readLine()) != null) {
                        stringBuffer.append(s);
                    }
                    throw new ScheduleException("Error on Perl Script " + script + "\nRun On Demand \n" + stringBuffer.toString());
                }

            } catch (ScheduleException ex) {
                // logFile.writeLine(ex.getMessage());
                ex.printStackTrace();
                ex.sendErrorEmail();
            } catch (IOException ex) {
                new ScheduleException(ex.getMessage()).sendErrorEmail();
            } catch (InterruptedException ex) {
                //  logFile.writeLine("Timeout has occured\n." + ex.getMessage());
                ex.printStackTrace();
            }

        }


        public void RunJava()
        {
            try {
                ProcessBuilder builder = new ProcessBuilder("java", script.replace(".class", ""));
                builder.directory(new File(ScheduleComponent.ScriptDirectory));
                builder.redirectErrorStream(true);
                Process p = builder.start();
                p.waitFor(25, TimeUnit.MINUTES);
                if (p.isAlive()) {
                    p.destroy();
                }
                int k = p.exitValue();
                if (k != 0) {
                    BufferedReader ErrorOutput = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                    StringBuffer stringBuffer = new StringBuffer();
                    String s;
                    while ((s = ErrorOutput.readLine()) != null) {
                        stringBuffer.append(s);
                    }
                    throw new ScheduleException("Error on Java Program " + script + "\nRun at On Demand\n\n" + stringBuffer.toString());

                }
            } catch (ScheduleException ex) {
                // logFile.writeLine(ex.getMessage());
                ex.printStackTrace();
                ex.sendErrorEmail();
            } catch (IOException ex) {
                new ScheduleException(ex.getMessage()).sendErrorEmail();
            } catch (InterruptedException ex) {
                // logFile.writeLine("Timeout has occured\n." + ex.getMessage());
                ex.printStackTrace();
            }

        }
    }
}