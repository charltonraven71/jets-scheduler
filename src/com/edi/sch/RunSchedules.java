package com.edi.sch;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;

public class RunSchedules
{
    Schedule schInfo;
    LogWriter logFile;
    String logFilename;

    public static PriorityQueue<String> LogQueue = new PriorityQueue<>();


    public RunSchedules(ArrayList<Schedule> schList, String filename)
    {

        for (Schedule run : schList) {
            Runnable runnable = new RunSchedulesThread(run,filename);
            Thread thread = new Thread(runnable);
            thread.start();
        }
    }
    class RunSchedulesThread implements Runnable{

        Schedule schInfo;
        String logFilename;

        RunSchedulesThread(Schedule schInfo, String logFile){
            this.schInfo = schInfo;
            this.logFilename = logFile;
        }

        @Override
        public void run()
        {
            SchedulerService.logThread.log(new LogMessage(logFilename,"Running Script: " + schInfo.getScript() + "-> " + Utilities.NowDate_MMddyyyy_hhmmss_a()+"   "+schInfo.getTimeQual()));


            //Perl
            if (schInfo.getScript().toLowerCase().endsWith(".pl")) {
                String command = "perl " + ScheduleComponent.ScriptDirectory + schInfo.getScript();
                RunScript(command);
            }

            //Java
            else if (schInfo.getScript().toLowerCase().endsWith(".class")) {
                RunJava();
            }


            //Python
            else if (schInfo.getScript().toLowerCase().endsWith(".py")) {
                String command = "python " + ScheduleComponent.ScriptDirectory + schInfo.getScript();
                RunScript(command);
            }

            //C#
            else if (schInfo.getScript().toLowerCase().endsWith(".csc")) {
                String command = "csc " + ScheduleComponent.ScriptDirectory + schInfo.getScript();
                RunScript(command);
            }

            //Executables
            else if (schInfo.getScript().toLowerCase().endsWith(".exe")) {
                String command = ScheduleComponent.ScriptDirectory + schInfo.getScript();
                RunScript(command);
            }

            //Batch
            else if (schInfo.getScript().toLowerCase().endsWith(".bat")) {
                String command = ScheduleComponent.ScriptDirectory + schInfo.getScript();
                RunScript(command);
            }
            //Bash (Unix)
            else if (schInfo.getScript().toLowerCase().endsWith(".sh")) {
                String command = ScheduleComponent.ScriptDirectory + schInfo.getScript();
                RunScript(command);
            }


            SchedulerService.logThread.log(new LogMessage(logFilename,"Script " + schInfo.getScript() + " Complete ->" + Utilities.NowDate_MMddyyyy_hhmmss_a()+"   "+schInfo.getTimeQual()));

        }


        public void RunScript(String command)
        {
            try {
                StringBuffer output = new StringBuffer();
                Process p;

                p = Runtime.getRuntime().exec(command);
                p.waitFor(25, TimeUnit.MINUTES);

                if (p.isAlive()) {
                    p.destroy();
                }

                if (p.exitValue() != 0) {
                    BufferedReader ErrorOutput = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                    StringBuffer stringBuffer = new StringBuffer();
                    String s;
                    while ((s = ErrorOutput.readLine()) != null) {
                        stringBuffer.append(s);
                    }
                    throw new ScheduleException("Error on Perl Script " + schInfo.getScript() + "\nRun at " + schInfo.runTime + "\n" + stringBuffer.toString());
                }

            } catch (ScheduleException ex) {
                // logFile.writeLine(ex.getMessage());
                ex.printStackTrace();
                ex.sendErrorEmail();
            } catch (IOException ex) {
                new ScheduleException(ex.getMessage()).sendErrorEmail();
            } catch (InterruptedException ex) {
                //  logFile.writeLine("Timeout has occured\n." + ex.getMessage());
                ex.printStackTrace();
            }

        }


        public void RunJava()
        {
            try {
                ProcessBuilder builder = new ProcessBuilder("java", schInfo.getScript().replace(".class", ""));
                builder.directory(new File(ScheduleComponent.ScriptDirectory));
                builder.redirectErrorStream(true);
                Process p = builder.start();
                p.waitFor(25, TimeUnit.MINUTES);
                if (p.isAlive()) {
                    p.destroy();
                }
                int k = p.exitValue();
                if (k != 0) {
                    BufferedReader ErrorOutput = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                    StringBuffer stringBuffer = new StringBuffer();
                    String s;
                    while ((s = ErrorOutput.readLine()) != null) {
                        stringBuffer.append(s);
                    }
                    throw new ScheduleException("Error on Java Program " + schInfo.getScript() + "\nRun at " + schInfo.runTime + "\n\n" + stringBuffer.toString());

                }
            } catch (ScheduleException ex) {
                // logFile.writeLine(ex.getMessage());
                ex.printStackTrace();
                ex.sendErrorEmail();
            } catch (IOException ex) {
                new ScheduleException(ex.getMessage()).sendErrorEmail();
            } catch (InterruptedException ex) {
                // logFile.writeLine("Timeout has occured\n." + ex.getMessage());
                ex.printStackTrace();
            }

        }
    }
}