package com.edi.sch;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class Schedule
{

    private String timeQual = "";
    private String day = "";
    private String time = "";
    private String script = "";
    private String description = "";
    public String runDate = "";
    public String runTime = "";
    public String calendarName = "";

    public Schedule(String timeQual, String multiValue, String time, String script, String description)
    {

        if (!timeQual.equals("C")) {
            this.timeQual = timeQual;

            this.day = day;
            if (day.toLowerCase().startsWith("x")) {
                this.day = "*";
            }


            this.day = multiValue;
            this.time = time;
            this.runTime = time;
            this.script = script;

            if (timeQual.equals("0") || timeQual.equals("5") || timeQual.equals("D")) {
                runDate = calcDaily();

            } else if (timeQual.equals("W")) {
                runDate = calcWeekly(day);

            } else if (timeQual.equals("M")) {
                runDate = calcMonthly(day);

            } else if (timeQual.equals("Y")) {
                runDate = calcYearly(day);

            }

            this.description = description;

        } else {
            this.timeQual = timeQual;
            this.script = script;
            this.calendarName = multiValue;
            this.time = time;
            this.description = "";
            this.runTime = time;

        }


    }


    public String calcCalendar()
    {


        return "";
    }
    public String calcDaily()
    {
        Date nowDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(nowDate);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int year = cal.get(Calendar.YEAR);
        Date date = new Date(year, month, day);
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");


        String strDate = format.format(new Date());
        return strDate;
    }

    private String calcWeekly(String dayOfWeek)
    {
        String daystr = "";
        if (dayOfWeek.equals("1")) {
            daystr = "SUNDAY";
        } else if (dayOfWeek.equals("2")) {
            daystr = "MONDAY";

        } else if (dayOfWeek.equals("3")) {
            daystr = "TUESDAY";

        } else if (dayOfWeek.equals("4")) {
            daystr = "WEDNESDAY";


        } else if (dayOfWeek.equals("5")) {
            daystr = "THURSDAY";

        } else if (dayOfWeek.equals("6")) {
            daystr = "FRIDAY";

        } else if (dayOfWeek.equals("7")) {
            daystr = "SATURDAY";

        }

        String strDate = "";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDateTime nowDate = LocalDateTime.now();


        if (String.valueOf(nowDate.getDayOfWeek()).toUpperCase().equals(daystr)) {
            strDate = formatter.format(nowDate);
        } else {
            LocalDateTime DummyDate = LocalDateTime.of(2013, 1, 1, 5, 5);
            strDate = formatter.format(DummyDate);
        }

        return strDate;
    }

    public String calcMonthly(String dayOfMonth)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

        int day = Integer.parseInt(dayOfMonth);
        Month month = LocalDateTime.now().getMonth();
        int year = LocalDateTime.now().getYear();

        LocalDateTime date = LocalDateTime.of(year, month, day, 3, 0);

        String strDate = formatter.format(date);


        return strDate;
    }

    public String calcYearly(String dayOfYear)
    {
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        Calendar cal = Calendar.getInstance();
        int test1 = Calendar.YEAR;

        cal.set(Calendar.DAY_OF_YEAR, Integer.parseInt(dayOfYear));
        String strDate = formatter.format(cal.getTime());

        return strDate;
    }

    public String getTimeQual()
    {
        return timeQual;
    }

    public String getDay()
    {
        return day;
    }

    public String getTime()
    {
        return time;
    }

    public String getScript()
    {
        return script;
    }

    public String getDescription()
    {
        return description;
    }

    public String  Print(){

        return "TimeQual: "+timeQual +"\nDay: "+day+"\nTime: "+time+"\nScript: "+script+"\nDescription: "+description;
    }

    public String getCalendarName()
    {
        return calendarName;
    }

    public void setCalendarName(String calendarName)
    {
        this.calendarName = calendarName;
    }
}
