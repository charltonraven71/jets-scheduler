package com.edi.sch;


import org.springframework.stereotype.Component;

import javax.xml.bind.SchemaOutputResolver;
import java.io.*;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * //***CRON FORMAT***
 * Field Name       Mandatory       Allowed Values      Allowed Special Characters
 * Seconds	        YES	            0-59	            , - * /
 * Minutes	        YES	            0-59	            , - * /
 * Hours	        YES	            0-23	            , - * /
 * Day of month	    YES	            1-31	            , - * ? / L W
 * Month	        YES	            1-12 or JAN-DEC 	, - * /
 * Day of week	    YES	            1-7 or SUN-SAT	    , - * ? / L #
 * Year	            NO	            empty, 1970-2099	, - * /
 * <p>
 * <p>
 * scheduled(cron = "[Seconds] [Minutes] [Hours] [Day of Month] [Month] [Day Of Week] [Year]")
 */
@Component("mainBean")
public class ScheduleComponent
{

    public static String schFile = "";
    public static String OnDemandFile = "";
    public static String PrimaryEmailFile = "";
    public static String ScriptDirectory = "";
    public static String CalendarDirectory = "";
    public static String LogDirectory = "";

    public ArrayList<Schedule> schs = new ArrayList<>();


    public ScheduleComponent(String schFile, String OnDemandFile, String PrimaryEmailFile, String ScriptDirectory, String CalendarDirectory, String LogDirectory)
    {
        this.schFile = schFile;
        this.OnDemandFile = OnDemandFile;
        this.PrimaryEmailFile = PrimaryEmailFile;
        this.ScriptDirectory = ScriptDirectory;
        this.CalendarDirectory = CalendarDirectory;
        this.LogDirectory = LogDirectory;

    }


    public ScheduleComponent()
    {
    }


    public void UpdateScheduler() throws IOException
    {
        schs.clear();
        BufferedReader br = new BufferedReader(new FileReader(ScheduleComponent.schFile));
        String line;
        while ((line = br.readLine()) != null) {
            if (!line.startsWith("#") && !line.equals("")) {
                String[] schArray = line.split("\\|");
                Schedule schedule = new Schedule(schArray[0].trim(), schArray[1].trim(), schArray[2].trim(), schArray[3].trim(), schArray[4].trim());
                schs.add(schedule);
            }
        }
        br.close();

    }


    public void executeOnTheZero() throws IOException, ScheduleException
    {
        Execute("0");
    }


    public void executeOnTheFive() throws IOException, ScheduleException
    {
        Execute("5");
    }

    public void executeDaily() throws IOException, ScheduleException
    {
        Execute("D");
    }

    public void executeWeekly() throws IOException, ScheduleException
    {
        Execute("W");
    }

    public void executeMonthly() throws IOException, ScheduleException
    {
        Execute("M");
    }

    public void executeYearly() throws IOException, ScheduleException
    {
        Execute("Y");
    }

    public void executeOnDemand() throws IOException, ScheduleException
    {
        ExecuteOnDemand();
    }
//    public void executeCalendar() throws IOException, ScheduleException
//    {
//        ExecuteCalendar();
//    }


    public void Execute(String time) throws IOException, ScheduleException
    {
        //Get current date
        Date currDate = new Date();
        String NowDate = Utilities.DateFormat_yyyyMMdd(currDate);
        String NowTime = Utilities.DateFormat_hhmm(currDate);


        //Create Schedule Thread for the Time slot
        GetSchedules ScheduleThread = new GetSchedules(time);

        //Get Regular Schedules
        ArrayList<Schedule> toRun = ScheduleThread.getSchedules(NowDate, NowTime,schs);


        //Get Calendar Schedules
        toRun.addAll(ScheduleThread.getCalendarSchedules(NowDate, NowTime, schs));



        //Run Schedules
        if (toRun.size() != 0) {
            //Get Filename for Log
            String LogDate1 = Utilities.DateFormat_yyyyMMddhhmmss(currDate);
            String LogDate2 = Utilities.DateFormat_MMddyyyy_hhmmss_a(currDate);
            String logFilename = Utilities.setLogFilename(time, LogDate1);

            logFilename = LogDirectory + logFilename;
            SchedulerService.logThread.log(new LogMessage(logFilename, "-----------------------  Running Job " + time + ":" + LogDate2 + "  -----------------------"));
            SchedulerService.logThread.log(new LogMessage(logFilename, "Running Schedules: " + Utilities.Schedules2String(toRun)));

            RunSchedules runSchedules = new RunSchedules(toRun, logFilename);

        }
    }

    public void ExecuteOnDemand() throws IOException
    {
        String time = "DEMAND";
        //Get current date
        Date currDate = new Date();
        String NowDate = Utilities.DateFormat_yyyyMMdd(currDate);
        String NowTime = Utilities.DateFormat_hhmm(currDate);


//        //Create Schedule Thread for the Time slot
//        GetSchedules ScheduleThread = new GetSchedules(time);
//
//        //Get Schedules
        ArrayList<String> OnDemands = new ArrayList<>();

        BufferedReader br = new BufferedReader(new FileReader(OnDemandFile));
        String line;
        while ((line = br.readLine()) != null) {
            if (!line.startsWith("#") || !line.trim().equals("")) {
                String script = line.trim();
                OnDemands.add(script);
            }
        }

        //Empty On Demand Schedule File
        PrintWriter writer = new PrintWriter(OnDemandFile);
        writer.print("");
        writer.close();


        //Run Schedules
        if (OnDemands.size() != 0) {
            //Get Filename for Log
            String LogDate1 = Utilities.DateFormat_yyyyMMddhhmmss(currDate);
            String LogDate2 = Utilities.DateFormat_MMddyyyy_hhmmss_a(currDate);
            String logFilename = Utilities.setLogFilename(time, LogDate1);

            logFilename = LogDirectory + logFilename;
            SchedulerService.logThread.log(new LogMessage(logFilename, "-----------------------  Running Job " + time + ":" + LogDate2 + "  -----------------------"));
            SchedulerService.logThread.log(new LogMessage(logFilename, "Running Schedules: " + Utilities.List2String(OnDemands)));

            RunOnDemandSchedules runSchedules = new RunOnDemandSchedules(OnDemands, logFilename);

        }
    }

    public void ExecuteCalendar() throws IOException, ScheduleException
    {
        String time = "C";

        //Get current date
        Date currDate = new Date();
        String NowDate = Utilities.DateFormat_yyyyMMdd(currDate);
        String NowTime = Utilities.DateFormat_hhmm(currDate);


        //Create Schedule Thread for the Time slot
        GetSchedules ScheduleThread = new GetSchedules(time);

        //Get Schedules
        ArrayList<Schedule> toRun = ScheduleThread.getCalendarSchedules(NowDate, NowTime, schs);


        //Run Schedules
        if (toRun.size() != 0) {
            //Get Filename for Log
            String LogDate1 = Utilities.DateFormat_yyyyMMddhhmmss(currDate);
            String LogDate2 = Utilities.DateFormat_MMddyyyy_hhmmss_a(currDate);
            String logFilename = Utilities.setLogFilename(time, LogDate1);

            logFilename = LogDirectory + logFilename;
            SchedulerService.logThread.log(new LogMessage(logFilename, "-----------------------  Running Job " + time + ":" + LogDate2 + "  -----------------------"));
            SchedulerService.logThread.log(new LogMessage(logFilename, "Running Schedules: " + Utilities.Schedules2String(toRun)));

            RunSchedules runSchedules = new RunSchedules(toRun, logFilename);

        }
    }


}
