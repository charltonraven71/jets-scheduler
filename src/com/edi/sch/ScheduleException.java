package com.edi.sch;


import com.edi.sonoco.SendEmail;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScheduleException extends Exception
{
    String Message;
    int exitCode;

    ScheduleException(String Message)
    {
        super(Message);

        this.Message = Message;
    }


    ScheduleException(String Message, int exitCode)
    {

        super(Message + "\n\n" + exitCode);
        this.Message = Message;
        this.exitCode = exitCode;

    }

    public void sendErrorEmail()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss a");
        Date nowDate = new Date();
        String nowDateStr = dateFormat.format(nowDate);
        String subject = "JETS Notification ERROR for " + Utilities.getComputerName() + " on " + nowDateStr;
        SendEmail errorEmail = new SendEmail(new File(ScheduleComponent.PrimaryEmailFile), subject, Message);
        errorEmail.Send();
    }

    public void sendErrorEmail(String Message)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss a");
        Date nowDate = new Date();
        String nowDateStr = dateFormat.format(nowDate);
        String subject = "Scheduler Service: JETS Notification ERROR on " + nowDateStr;
        SendEmail errorEmail = new SendEmail(new File(ScheduleComponent.PrimaryEmailFile), subject, Message);
        errorEmail.Send();
    }


    public void sendErrorEmailAndExit()
    {

        if (exitCode == 100) {
            System.exit(exitCode);
        }
        if (exitCode > 500) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss a");
            Date nowDate = new Date();
            String nowDateStr = dateFormat.format(nowDate);
            String subject = "Scheduler Service: JETS Notification ERROR on " + nowDateStr;
            SendEmail errorEmail = new SendEmail(new File(ScheduleComponent.PrimaryEmailFile), subject, Message);
            errorEmail.Send();
            System.out.println("Email Sent");
            System.exit(exitCode);
        }
    }

}
