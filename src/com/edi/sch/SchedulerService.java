package com.edi.sch;


import com.edi.sonoco.SendEmail;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
* <h1>JETS - Java Edi Task Scheduler</h1>
* The Java program reads a XML context file that
 * holds program parameters and schedules. The
 * program uses a table that reads scheduled scripts
 * to run on the listed schedule.
*
 * @author Charlton Williams
 * @version 1.0
 * @since 08/22/2018

*/

public class SchedulerService
{

    public static BlockingQueue<LogMessage> LogQueue = new ArrayBlockingQueue<>(4);
    public static  LogThread logThread = null;

    //JETS - Java EDI Task Scheduler
    public static void main(String[] args) throws ScheduleException
    {
        //---------------------------------Create appContext.xml IF NOT EXIST-------------------------------------------
        String JetsContextXML = "JETSConfiguration/JETSContext.xml";
        File AppContextXMLFile = new File(JetsContextXML);
        if (!AppContextXMLFile.exists()) {

            AppContextXMLFile.getParentFile().mkdirs();
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(AppContextXMLFile));
                bw.write(Utilities.CreateXMLFile());
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //--------------------------------------------------------------------------------------------------------------


        //Start JETS Scheduler
        ApplicationContext context = new FileSystemXmlApplicationContext(JetsContextXML);
        ScheduleComponent scheduleComponent = context.getBean("ScheduleParams",ScheduleComponent.class);

        if(!new File(scheduleComponent.schFile).exists()){
            throw new ScheduleException("Schedule File does not exist. Either create the Schedule file or specify a new Schedule file in the JETSContext.xml file", 100);
        }
        if (!new File(scheduleComponent.OnDemandFile).exists()) {
            throw new ScheduleException("On Demand File does not exist. Either create the OnDemand file or specify a new On Demand file in the JETSContext.xml file", 100);
        }
        if(!new File(scheduleComponent.ScriptDirectory).exists()){
            throw new ScheduleException("Scripts Directory does not exist. Either create the Script Directory or specify a new Directory in the JETSContext.xml file", 100);
        }

        if(!new File(scheduleComponent.LogDirectory).exists()){
            throw new ScheduleException("Directory does not exist. Either create the Log Dicrectory or specify a new Directory in the JETSContext.xml file", 100);
        }

        if (!new File(scheduleComponent.CalendarDirectory).exists()) {
            throw new ScheduleException("Calendar Directory does not exist. Either create the Calendar Dicrectory or specify a new Directory in the JETSContext.xml file", 100);
        }

        if(!new File(scheduleComponent.PrimaryEmailFile).exists()){
            throw new ScheduleException("Email File does not exist. Either create the Primary Email File or specify a new Directory in the JETSContext.xml file", 100);
        } else {
            String subject = "JETS Notification for " + Utilities.getComputerName() + ": Scheduler Started at " + Utilities.NowDate_FullDayAndDate();
            String message = "JETS Notification: Scheduler Started at " + Utilities.NowDate_FullDayAndDate();
            SendEmail startEmail = new SendEmail(new File(ScheduleComponent.PrimaryEmailFile), subject, message);
            startEmail.Send();
        }

        logThread = new LogThread();
        System.out.println();
        try {
            scheduleComponent.UpdateScheduler();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
