package com.edi.sch;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Utilities
{

    public static String getComputerName()
    {
        String hostname = "";
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            System.out.println("Unable to get Computer name");
        }
        return hostname;
    }

    public static String NowDate_FullDayAndDate()
    {

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEEE, MMMMM yyyy hh:mm:ss aa");
        String nowDateStr = dateFormat.format(date);
        return nowDateStr;
    }
    public static String NowDate_yyyyMMddhhmmss()
    {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        String nowDateStr = dateFormat.format(date);
        return nowDateStr;
    }

    public static String NowDate_MMddyyyy_hhmmss_a()
    {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss a");
        String nowDateStr = dateFormat.format(date);
        return nowDateStr;
    }
    public static String DateFormat_MMddyyyy_hhmmss_a(Date date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss a");
        String nowDateStr = dateFormat.format(date);
        return nowDateStr;
    }

    public static String DateFormat_yyyyMMddhhmmss(Date date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        String nowDateStr = dateFormat.format(date);
        return nowDateStr;
    }

    public static String DateFormat_yyyyMMdd(Date date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String nowDateStr = dateFormat.format(date);
        return nowDateStr;
    }

    public static String DateFormat_hhmm(Date date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HHmm");
        String nowDateStr = dateFormat.format(date);
        return nowDateStr;
    }

    public static String Schedules2String(List<Schedule> schList)
    {
        String str = "\n";
        for (Schedule sch : schList) {
            str += sch.getScript() + "\n";
        }

        return str;
    }

    public static String List2String(List<String> ScriptList)
    {
        String str = "\n";
        for (String script : ScriptList) {
            str += script + "\n";
        }

        return str;
    }
    public static String setLogFilename(String time, String date)
    {
        time = "_" + time + "_";
        String logFilename = "JETS_" + time + "_" + date + ".log";
        return logFilename;
    }

    public static String CreateXMLFile()
    {
        String XMLFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<beans xmlns=\"http://www.springframework.org/schema/beans\"\n" +
                "       xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:context=\"http://www.springframework.org/schema/context\"\n" +
                "       xmlns:task=\"http://www.springframework.org/schema/task\"\n" +
                "       xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd\n" +
                "        \t\t\t\t\thttp://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.0.xsd\n" +
                "        \t\t\t\t\thttp://www.springframework.org/schema/task http://www.springframework.org/schema/task/spring-task-4.0.xsd\">\n" +
                "\n" +
                "\n" +
                "    <!--JETS : (J)ava (E)di (T)ask (S)cheduler-->\n" +
                "    <context:component-scan base-package=\"com.edi.sch\" />\n" +
                "\n" +
                "\n" +
                "    <!--Please see Cron examples at https://crontab.guru/examples.html -->\n" +
                "    <!--***CRON FORMAT***-->\n" +
                "    <!--Field Name      Mandatory       Allowed Values      Allowed Special Characters-->\n" +
                "    <!--Seconds\t        YES\t            0-59\t            , - * /-->\n" +
                "    <!--Minutes\t        YES\t            0-59\t            , - * /-->\n" +
                "    <!--Hours\t        YES\t            0-23\t            , - * /-->\n" +
                "    <!--Day of month\tYES\t            1-31\t            , - * ? / L W-->\n" +
                "    <!--Month\t        YES\t            1-12 or JAN-DEC \t, - * /-->\n" +
                "    <!--Day of week\t    YES\t            1-7 or SUN-SAT\t    , - * ? / L #-->\n" +
                "    <!--Year\t        NO\t            empty, 1970-2099\t, - * /-->\n" +
                "\n" +
                "    <!--@scheduled(cron = \"[Seconds] [Minuites] [Hours] [Day of Month] [Month] [Day Of Week] [Year]\")-->\n" +
                "\n" +
                "\n" +
                "\n" +
                "    <!-- \tConstructor with Three Parameter -->\n" +
                "    <!--FILES AND FILE PATHS-->\n" +
                "    <!--Schedule File:     0  -->\n" +
                "    <!--SCRIPTS DIRECTORY: 1  -->\n" +
                "    <!--LOG DIRECTORY:     2  -->\n" +
                "    <bean id=\"ScheduleParams\" class = \"com.edi.sch.ScheduleComponent\" >\n" +
                "        <constructor-arg index=\"0\"\n" +
                "                         value=\"C:\\Users\\63530\\Projects\\scheduler\\JETS\\schedule\\JETS_SchTable.dat\"/>     <!--Schedule Table-->\n" +
                "        <constructor-arg index=\"1\"\n" +
                "                         value=\"C:\\Users\\63530\\Projects\\scheduler\\JETS\\schedule\\JETS_SchOnDemand.dat\"/>  <!--OnDemand Table-->\n" +
                "        <constructor-arg index=\"2\"\n" +
                "                         value=\"C:\\Users\\63530\\Projects\\scheduler\\\\Email\\JETS0001.txt\"/>                 <!--Primary Email Table for Errors-->\n" +
                "        <constructor-arg index=\"3\"\n" +
                "                         value=\"C:\\scripts\\\"/>                                                            <!--Script Directory-->\n" +
                "        <constructor-arg index=\"4\"\n" +
                "                         value=\"C:\\Users\\63530\\Projects\\scheduler\\JETS\\schedule\\Calendars\\\"/>              <!--Log Directory-->\n" +
                "        <constructor-arg index=\"5\"\n" +
                "                         value=\"C:\\Users\\63530\\Projects\\scheduler\\JETS\\logs\\\"/>                             <!--Email Tables Directory-->\n" +
                "    </bean>\n" +
                "\n" +
                "    <!--Scheduled Task-->\n" +
                "    <task:scheduled-tasks scheduler=\"JETS\">\n" +
                "\n" +
                "        <task:scheduled ref=\"mainBean\" method=\"UpdateScheduler\"\n" +
                "                        cron=\"30 * * * * ?\"/>                          <!--KEEP-->\n" +
                "        <task:scheduled ref=\"mainBean\" method=\"executeDaily\" cron=\"1 0/5 * * * ?\"/>                   <!--D-->\n" +
                "        <task:scheduled ref=\"mainBean\" method=\"executeYearly\" cron=\"1 0/5 * * * ?\"/>                           <!--Y-->\n" +
                "        <task:scheduled ref=\"mainBean\" method=\"executeMonthly\" cron=\"1 0/5 * * * ?\"/>                          <!--M-->\n" +
                "        <task:scheduled ref=\"mainBean\" method=\"executeWeekly\" cron=\"1 0/5 * * * ?\"/>                           <!--W-->\n" +
                "        <task:scheduled ref=\"mainBean\" method=\"executeOnTheZero\" cron=\"1 0,10,20,30,40,50 * * * ?\"/>           <!--0-->\n" +
                "        <task:scheduled ref=\"mainBean\" method=\"executeOnTheFive\" cron=\"1 5,15,25,35,45,55 * * * ?\"/>           <!--5-->\n" +
                "        <task:scheduled ref=\"mainBean\" method=\"executeOnDemand\"\n" +
                "                        cron=\"1 0/5 * * * ?\"/>                         <!--ON DEMAND-->\n" +
                "\n" +
                "    </task:scheduled-tasks>\n" +
                "    <task:scheduler id=\"JETS\" pool-size=\"10\"/>\n" +
                "</beans>";

        return XMLFile;
    }

}
